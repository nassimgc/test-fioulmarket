<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewsApiService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getNewsImages(String $apiCountry, String $apiKey): array
    {
        // Déclaration du tableau qui contiendra les images
        $newsImages = array();

        // Appel de l'API NewsApi
        $newsApiResponse = $this->client->request(
            'GET',
            'https://newsapi.org/v2/top-headlines?country='.$apiCountry.'&apiKey='.$apiKey
        );

        // Récupération des données de l'API (au format JSON)
        $newsData  = json_decode($newsApiResponse->getContent());

        $imageExtensionsList = array(
            'gif',
            'jpg',
            'jpeg',
            'png'
        );

        $regexImageExtensions = '/^[^?]*\.(jpg|jpeg|gif|png)(?![\w.\-_])/i';

        // Parcours des articles de l'API
        for ($articleNb = 0; $articleNb < count($newsData->articles); $articleNb++) {
            // Récupération de l'URL de l'article
            $url = $newsData->articles[$articleNb]->url;
           // Vérification de la présence d'une image dans l'article
            if (
                    !empty($newsData->articles[$articleNb]->urlToImage) 
                    && filter_var($url, FILTER_VALIDATE_URL) 
                    && preg_match($regexImageExtensions, $newsData->articles[$articleNb]->urlToImage)
                    && in_array( strtolower(pathinfo($newsData->articles[$articleNb]->urlToImage)['extension']), $imageExtensionsList)
                ) 
            {
                // Ajout de l'image et du lien dans le tableau
                $newsImages[$articleNb]['link'] = $url;
                $newsImages[$articleNb]['image'] = iconv('utf-8', 'latin1', $newsData->articles[$articleNb]->urlToImage);
            }
        }

        // Retour du tableau contenant les images
        return $newsImages;
    }
}

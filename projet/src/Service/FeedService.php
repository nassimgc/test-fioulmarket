<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class FeedService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getComicImages(): array
    {
            // Initialisation du tableau final
            $comicImages = array();

            // Récupération du flux RSS
            $feedResponse = $this->client->request(
                'GET',
                'http://www.commitstrip.com/en/feed/'
            );

            // Récupération du contenu du flux RSS (format XML)
            $feedXml = simplexml_load_string($feedResponse->getContent(), 'SimpleXMLElement', LIBXML_NOCDATA);

            $imageExtensionsList = array(
                'gif',
                'jpg',
                'jpeg',
                'png'
            );
            
            $regexImageExtensions = '/^[^?]*\.(jpg|jpeg|gif|png)(?![\w.\-_])/i';

            // Récupération du channel
            $channel = $feedXml->channel;
            // Récupération du nombre d'item du channel
            $numberChannelItem = count($feedXml->channel->item);

            // Itération des items du channel
            for ($itemNb = 0; $itemNb < $numberChannelItem; $itemNb++) {
                // Récupération du contenu de l'item
                $doc = new \DomDocument();
                // Chargement du contenu de l'item dans le DOM
                $doc->loadHTML((string)$channel->item[$itemNb]->children("content", true));
                $xpath = new \DomXpath($doc);
                // Récupération de l'attribut src de l'image
                $xq = $xpath->query('//img[contains(@class,"size-full")]/@src');
                // Récupération de la valeur de l'attribut src
                $src = $xq[0]->value;

                // Si l'image est valide, on l'ajoute au tableau final
                if (
                        !empty($src) 
                        && filter_var($channel->item[$itemNb]->link, FILTER_VALIDATE_URL) 
                        && preg_match($regexImageExtensions, $src)
                        && in_array( strtolower(pathinfo($src)['extension']), $imageExtensionsList)
                    )
                {
                    $comicImages[$itemNb]['link'] = strip_tags($channel->item[$itemNb]->link);
                    $comicImages[$itemNb]['image'] = iconv('utf-8', 'latin1', $src);
                }
            }

        // Retourne le tableau lien-image
        return $comicImages;
    }
}

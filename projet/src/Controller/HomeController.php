<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\NewsApiService;
use App\Service\FeedService;


class HomeController extends AbstractController
{

    private $feedService;
    private $newsApiService;

    public function __construct(FeedService $feedService, NewsApiService $newsApiService)
    {
        $this->feedService = $feedService;
        $this->newsApiService = $newsApiService;
    }
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function __invoke() : Response
    {
        try {
            // Récupération des images provenant de news api
            $newsImages = $this->newsApiService->getNewsImages($this->getParameter('apicountry'), $this->getParameter('apikey') );
            // Récupération des images provenant de commitstrip
            $comicImages = $this->feedService->getComicImages();
            // Fusion des deux tableaux
            $finalContent = array_merge_recursive($newsImages, $comicImages);

            // Renvoi de la vue
            return $this->render('default/index.html.twig', array('contents' => $finalContent));
        } catch (\Exception $e) {
            throw new \Exception('Un problème est survenu dans l\'import d\'images !');
        }
    }
}

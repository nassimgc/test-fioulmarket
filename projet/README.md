test-dev
========

### Commande pour lancer l'application sur docker:
- Se placer à la racine du projet (là où est placé le docker-compose.yaml)
- Option 1 : Lancer la commande à l'aide du makefile en tapant la commande make up
- Option 2 : Lancer la commande docker compose up --build -d 
- Option 3 : récupérer directement le projet dans le répertoire "projet"
- Configuration des variables d'environnements dans .env.local : 
	- ```
        API_KEY=yourApiKey
        API_COUNTRY=selectCountry (exemple : en)
		```

Un stagiaire à créer le code contenu dans le fichier src/Controller/Home.php

Celui permet de récupérer des urls via un flux RSS ou un appel à l’API NewsApi. 
Celles ci sont filtrées (si contient une image) et dé doublonnées. 
Enfin, il faut récupérer une image sur chacune de ces pages.

Le lead dev n'est pas très satisfait du résultat, il va falloir améliorer le code.

Pratique : 
1. Revoir complètement la conception du code (découper le code afin de pouvoir ajouter de nouveaux flux simplement) 

Questions théoriques : 
1. Que mettriez-vous en place afin d'améliorer les temps de réponses du script
- L'utilisation de méthode Asynchrone
- Mettre en cache les résultats les plus récents et les plus utlisés particulièrement avec une BDD NoSQL et mise en cache comme REDIS
2. Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)
- L'utilisation d'une pagination afin de ne pas surcharger le navigateur
- L'utilisation de CDN pour la mise en cache et la réduction de la consommation de la bande passante.
- Mise en place d'outils permettant de réaliser une file d'attente (exemple : RabbitMQ).

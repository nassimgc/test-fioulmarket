FROM php:7.4-apache
RUN apt-get update -y \
&& apt-get -y install git acl unzip


# Installe les extensions php (voir liste : https://github.com/mlocati/docker-php-extension-installer)
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions intl pdo opcache zip calendar dom mbstring pdo_mysql pdo_pgsql

COPY --from=composer/composer:latest-bin /composer /usr/local/bin/composer

COPY ./projet /var/www/projet

# Installation de NVM (pour NodeJS)
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

RUN export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")" && \
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
nvm install --lts && \
nvm use --lts

WORKDIR /var/www/projet/
RUN composer install && \
php bin/console c:c

# # Permet l'écriture dans le répertoire var et logs 
RUN HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1) && \
 setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var && \
 setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
